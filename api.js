var Server = require('blklab-server').Server;
var Auth = require('blklab-server').Auth;
var Promise = require('promise');
var Security = require('./src/lib/security');

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/storysnap-v2');

require('./src/models');
require('./src/routes');

var server = new Server({
    cors: true
}).listen(4014, '127.0.0.1');

Auth.check = function(req, resolve, reject){
    Security.verify(req.headers['x-access-token']).then(function(user){
        req.auth_id = user.id;
        req.auth_type = user.type;
        resolve(user);
    }, function(){
        reject({msg: 'Unauthorized'});
    })
};
