var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/storysnap-v2');

require('./src/models');

var Users = mongoose.model('Users');
var Stats = mongoose.model('Stats');

var types = {
    "Account Creation": 0,
    "Story Favorited": 1,
    "Story Purchase": 2,
    "Story Record Launch": 3,
    "Completion of Record": 4,
    "Shared Story": 5,
    "Invitation of Non-User to install": 6,
    "Invitation of a non-user to read story": 7,
    "Reminder of Invitation": 8,
    "Withdrawl of Invitation": 9,
    "Stories received for review": 10,
    "Stories reviewed and accepted": 11,
    "Inviation received": 12,
    "Reminder received": 13,
    "Inviation accepted": 14,
    "Inviation declined": 15,
    "Story Playback": 16,
    "Export audio to MP3": 17,
    "Session Start": 18,
    "Session End": 19,
    "Details Viewed": 20,
    "Tips turned off": 21,
    "Tips turned on": 22
}

Object.keys(types).forEach(function(key){
    var idx = types[key];
    Stats.find({action: idx}, function(err, row){
        if(!row){
            var stat = new Stats();
            stat.action = idx;
            stat.save();
        }
    })
})

Users.find(function(err, users){
    users.forEach(function(user){
        console.log(user.email);
    })
    process.exit(0);
});
