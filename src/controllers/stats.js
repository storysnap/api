var fs = require('fs');
var sys = require('sys');
var Security = require('../lib/security');
var Config = require('../config/config');
var jwt    = require('jsonwebtoken');
var moment = require('moment');
var uuid = require('uuid');
var Mailgun = require('mailgun-js');
var mongoose = require('mongoose');
var Stories = mongoose.model('Stories');
var Users = mongoose.model('Users');
var Stats = mongoose.model('Stats');
var Sendgrid = require('sendgrid')("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");

var mailgun = new Mailgun({
    apiKey: 'key-9dao0tj6p33aypwkko5ksg88s2g2wqs4',
    domain: 'mykubby.mg.lackner-buckingham.com'
});

var internals = {};

exports = module.exports = internals.ProfessorsController = {
    filterBy: function(req, res){
        Users.find(function(err, users){
            var output = [];
            if(req.params.filter == "profiles"){
                users.forEach(function(user){
                    output.push({
                        first_name: user.first_name,
                        last_name: user.last_name,
                        email: user.email
                    });
                });
            }

            if(req.params.filter == "no-action"){
                users.forEach(function(user){
                    if(user.active){
                        if(user.activity.length == 0){
                            output.push({
                                first_name: user.first_name,
                                last_name: user.last_name,
                                email: user.email
                            });
                        }
                    }
                });
            }

            if(req.params.filter == "purchase-not-recorded"){
                users.forEach(function(user){
                    var storiesFinished = [];
                    var receiptsIds = user.receipts.map(function(r){
                        return r.story;
                    })
                    user.activity.forEach(function(row){
                        if(row.action.toLowerCase().indexOf('finished recording') != -1){
                            receiptsIds.forEach(function(id){
                                 if(id == row.story){
                                    storiesFinished.push(id);
                                }
                            });
                        }
                    });

                    if(storiesFinished.length != receiptsIds.length){
                        output.push({
                            first_name: user.first_name,
                            last_name: user.last_name,
                            email: user.email
                        });
                    }


                });
            }

            if(req.params.filter == "recorded-not-shared"){
                var existingUsers = [];
                users.forEach(function(user){
                    user.queue.forEach(function(row){
                        if(!row.shared_with){
                            row.shared_with = [];
                        }
                        if(row.type == 4 && row.shared_with.length == 0 && existingUsers.indexOf(user.email) == -1){
                            existingUsers.push(user.email);
                            output.push({
                                first_name: user.first_name,
                                last_name: user.last_name,
                                email: user.email
                            });
                        }
                    });
                });
            }

            if(req.params.filter == "recorded-and-shared"){
                var existingUsers = [];
                users.forEach(function(user){
                    user.queue.forEach(function(row){
                        if(!row.shared_with){
                            row.shared_with = [];
                        }
                        if(row.type == 4 && row.shared_with.length > 0 && existingUsers.indexOf(user.email) == -1){
                            existingUsers.push(user.email);
                            output.push({
                                first_name: user.first_name,
                                last_name: user.last_name,
                                email: user.email
                            });
                        }
                    });
                });
            }

            if(req.params.filter == "invite-sent"){
                var existingUsers = [];
                users.forEach(function(user){
                    user.activity.forEach(function(row){
                        if(row.action.toLowerCase().indexOf('invited') != -1 && row.action.toLowerCase().indexOf('to read story') != -1 && existingUsers.indexOf(user.email) == -1){
                            existingUsers.push(user.email);
                            output.push({
                                first_name: user.first_name,
                                last_name: user.last_name,
                                email: user.email
                            });
                        }
                    });
                });
            }

            if(req.params.filter == "invite-accepted"){
                var existingUsers = [];
                users.forEach(function(user){
                    user.activity.forEach(function(row){
                        if(row.action.toLowerCase().indexOf('accepted invite') != -1 && existingUsers.indexOf(user.email) == -1){
                            existingUsers.push(user.email);
                            output.push({
                                first_name: user.first_name,
                                last_name: user.last_name,
                                email: user.email
                            });
                        }
                    });
                });
            }

            if(req.params.filter == "invite-accepted-and-recorded"){
                users.forEach(function(user){
                    var storiesFinished = [];
                    var acceptedInvites = [];
                    var receiptsIds = user.receipts.map(function(r){
                        return r.story;
                    })
                    user.activity.forEach(function(row){
                        if(row.action.toLowerCase().indexOf('accepted invite') != -1){
                            acceptedInvites.push(row.story.toString().trim());
                        }

                        if(row.action.toLowerCase().indexOf('finished recording') != -1){
                            storiesFinished.push(row.story.toString().trim());
                        }
                    });

                    if(acceptedInvites.length > 0 && storiesFinished.length > 0){
                        var sharedOne = false;
                        console.log('');
                        console.log(storiesFinished);

                        acceptedInvites.forEach(function(id){
                            console.log(id);
                            console.log(storiesFinished.indexOf(id));
                            console.log('');
                            if(storiesFinished.indexOf(id) != -1){
                                sharedOne = true;
                            }
                        })

                        if(sharedOne){
                            output.push({
                                first_name: user.first_name,
                                last_name: user.last_name,
                                email: user.email
                            });
                        }
                    }
                });
            }


            res.send(output);
        });
    },

    totals: function(req, res){
        var data = {
            totalUsers: 0,
            totalUsers: 0,
            totalUsersActivated: 0,
            totalUsersShared: 0,
            totalUsersInvited: 0,
            totalUsersRecorded: 0,
            totalUsersPurchasedStory: 0,
            totalUsersActivatedNoAction: 0,
            totalUsersFullProfile: 0,
            totalUsersProfileImage: 0,
            totalUsersAcceptedInvites: 0,
            totalUsersWithdrawInvites: 0,
            totalUsersActivatedPercent: 0,
            totalUsersSharedPercent: 0,
            totalUsersInvitedPercent: 0,
            totalUsersRecordedPercent: 0,
            totalUsersPurchasedStoryPercent: 0,
            totalUsersActivatedNoActionPercent: 0,
            totalUsersFullProfilePercent: 0,
            totalUsersProfileImagePercent: 0,
            totalUsersAcceptedInvitesPercent: 0,
            totalUsersWithdrawInvitesPercent: 0,
        }
        Users.find(function(err, users){
            data.totalUsers = users.length;
            users.forEach(function(user){
                if(!user.activity){
                    user.activity = [];
                }

                if(!user.receipts){
                    user.receipts = [];
                }

                if(user.receipts.length == 0){
                    data.totalUsersPurchasedStory += 1;
                }

                if(user.active){
                    data.totalUsersActivated += 1;
                    if(user.activity.length == 0){
                        data.totalUsersActivatedNoAction += 1;
                    }
                }


                var profileFields = [
                    "first_name",
                    "last_name",
                    "email",
                    "title",
                    "birth_year",
                    "languages",
                    "country",
                    "favorite_books",
                    "favorite_books_to_read",
                    "pin",
                    "password",
                    "profileImage"
                ]

                var fullProfile = false;
                var hasProfile = 0;
                profileFields.forEach(function(key){
                    if(user[key] && user[key] != "" && user[key] != "default.png"){
                        hasProfile++;
                    }
                });

                if(hasProfile == profileFields.length){
                    fullProfile = true;
                    data.totalUsersFullProfile += 1;
                }

                if(user.profileImage != "default.png"){
                    data.totalUsersProfileImage += 1;
                }


                var foundShare = false;
                var foundInvite = false;
                var foundRecording = false;
                var foundAcceptedInvite = false;
                var foundWithdrawInvite = false;

                user.activity.forEach(function(row){
                    if(row.action.toLowerCase().indexOf('shared recording with') != -1 && !foundShare){
                        data.totalUsersShared += 1;
                        foundShare = true;
                    }

                    if(row.action.toLowerCase().indexOf('invited') != -1 && row.action.toLowerCase().indexOf('to read story') != -1 && !foundInvite){
                        data.totalUsersInvited += 1;
                        foundInvite = true;
                    }

                    if(row.action.toLowerCase().indexOf('finished recording') != -1 && !foundRecording){
                        data.totalUsersRecorded += 1;
                        foundRecording = true;
                    }

                    if(row.action.toLowerCase().indexOf('accepted invite') != -1 && !foundRecording){
                        data.totalUsersAcceptedInvites += 1;
                        foundAcceptedInvite = true;
                    }

                    if(row.action.toLowerCase().indexOf('revoked invite to') != -1 && !foundRecording){
                        data.totalUsersWithdrawInvites += 1;
                        foundWithdrawInvite = true;
                    }
                });
            });

            data.totalUsersActivatedPercent = ((data.totalUsersActivated/data.totalUsers).toFixed(2) * 100) + '%';
            data.totalUsersSharedPercent = ((data.totalUsersShared/data.totalUsers).toFixed(2) * 100) + '%';
            data.totalUsersInvitedPercent = ((data.totalUsersInvited/data.totalUsers).toFixed(2) * 100) + '%';
            data.totalUsersRecordedPercent = ((data.totalUsersRecorded/data.totalUsers).toFixed(2) * 100) + '%';
            data.totalUsersPurchasedStoryPercent = ((data.totalUsersPurchasedStory/data.totalUsers).toFixed(2) * 100) + '%';
            data.totalUsersActivatedNoActionPercent = ((data.totalUsersActivatedNoAction/data.totalUsersActivated).toFixed(2) * 100) + '%';
            data.totalUsersFullProfilePercent = ((data.totalUsersFullProfile/data.totalUsers).toFixed(2) * 100) + '%';
            data.totalUsersProfileImagePercent = ((data.totalUsersProfileImage/data.totalUsers).toFixed(2) * 100) + '%';
            data.totalUsersAcceptedInvitesPercent = ((data.totalUsersAcceptedInvites/data.totalUsersInvited).toFixed(2) * 100) + '%';
            data.totalUsersWithdrawInvitesPercent = ((data.totalUsersWithdrawInvites/data.totalUsersInvited).toFixed(2) * 100) + '%';
            res.send(data);
        });
    },

    users: function(req, res){

    },

    stories: function(req, res){

    },
}
