var fs = require('fs');
var sys = require('sys');
var Security = require('../lib/security');
var Config = require('../config/config');
var jwt    = require('jsonwebtoken');
var moment = require('moment');
var uuid = require('uuid');
var _mailgun = require('mailgun-js');
var mongoose = require('mongoose');
var Stories = mongoose.model('Stories');
var Users = mongoose.model('Users');
var Sendgrid = require('sendgrid')("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");
var gm = require('gm');
var PDFImage = require("pdf-image").PDFImage;
var PngQuant = require('pngquant')
var RWC = require('random-weighted-choice');

var Mailgun = new _mailgun({
    apiKey: 'key-9dao0tj6p33aypwkko5ksg88s2g2wqs4',
    domain: 'mykubby.mg.lackner-buckingham.com'
});

var internals = {};

exports = module.exports = internals.ProfessorsController = {
    find: function(req, res){
        Stories.find({active: true}).sort('-date_added').exec(function(err, stories){
            stories = stories || [];
            res.send(stories);
        });
    },

    findProducts: function(req, res){
        Stories.find({active: true}).exec(function(err, stories){
            var ids = [];
            stories.forEach(function(row){
                if(row.apple_id && row.apple_id != ""){
                    ids.push(row.apple_id)
                }
            });
            res.send(ids);
        });
    },

    findThumbnails: function(req, res){
        Stories.find({active: true}, 'featured').exec(function(err, stories){
            res.send(stories);
        });
    },

    discover: function(req, res){
        Stories.find({active: true}).sort('-date_added').exec(function(err, stories){
            Users.findOne({_id:req.auth_id}, function(err, user){
                stories = stories || [];
                var struct = {};
                stories.forEach(function(row, idx){
                    row.weight = row.weight ? row.weight : 1;
                    struct[row._id] = idx;
                })

                var featured = [];
                var choosen = [];
                var cnt = 0;
                while(cnt < 4){
                    var id = RWC(stories);
                    var idx = struct[id];
                    if(choosen.indexOf(idx) == -1){
                        featured.push(stories[idx]);
                        choosen.push(idx);
                        cnt++;
                    }
                }

                var recent = stories.slice(0,5);
                var popular = stories.sort(function(a, b){
                    if(a.popularity < b.popularity) return -1;
                    if(a.popularity > b.popularity) return 1;
                    return 0;
                }).slice(0,5);

				if(user){
	                res.send({
	                    all:stories,
	                    featured: featured,
	                    recent: recent,
	                    popular: popular,
	                    favorites: user.favorites
	                });
				}else{
					res.send({
	                    all:stories,
	                    featured: featured,
	                    recent: recent,
	                    popular: popular,
	                    favorites: []
	                });
				}
            });
        });
    },

    findAll: function(req, res){
        Stories.find().sort('-date_added').exec(function(err, stories){
            stories = stories || [];
            res.send(stories);
        });
    },

    findFiltered: function(req, res){
        Stories.find({active: true}).sort('-date_added').exec(function(err, stories){
            stories = stories || [];
            res.send(stories);
        });
    },

    findById: function(req, res){
        Stories.findById(req.params.id).exec(function(err, story){
            story = story|| [];
            res.send(story);
        });
    },

    create: function(req, res){
        var model = new Stories(req.body);
        model.save(function(err, story){
            res.send(story);
        });
    },

    update: function(req, res){
        Stories.update({_id: req.params.id}, req.body, function(err, data){
            if(err){
                res.send({error:err});
            }else{
                res.send(data);
            }
        });
    },

    process: function(req, res){
        console.log(req.body);
        var path = req.files.files.path;
        var pdfImage = new PDFImage(path);
        var pages = [];

        var params = {
            "-density": 300,
            "-resize": '2048x2048'
        };

        if(parseInt(req.body.cropmarks) == 1){
            params["-define"] = "pdf:use-trimbox=true";
        }

        pdfImage.setConvertOptions(params);
        pdfImage.numberOfPages().then(function (numberOfPages) {
            Stories.findById(req.params.id).exec(function(err, story){
                var idx = 0;
                story.images = [];
                while(idx < numberOfPages){
                    idx++;
                    story.images.push(req.params.id + '-' + idx + '.png')
                }
                story.images.push("FlyAwayHorse_IMAGE_PAGE_end.png")
                story.pages = numberOfPages;
                story.save(function(err){
                    console.log('Processing ' + numberOfPages + ' Pages');
                    var i = 0;
                    var next = function(){
                        if(i < numberOfPages){
                            console.log('Starting Page ' + i);
                            pdfImage.setConvertExtension("png");
                            pdfImage.convertPage(i).then(function (imagePath) {
                                i++;
                                var tmp = req.params.id + '-' + i + '.png';//imagePath.split('/').pop();
                                var o = fs.createWriteStream('/var/www/client/app/assets/test/' + tmp);
                                fs.createReadStream(imagePath).pipe(o);
                                //.pipe(new PngQuant())
                                o.on('finish', function(){
                                    pages.push(tmp);
                                    console.log('Page ' + (i) + ' Processed');
                                    next();
                                });
                            }, function(){
                                console.log('error');
                            });

                        }else{
                            fs.unlink(path, function(){
                                var data = {
                                    from: 'no-reply@storysnap.org',
                                    to: 'baird@lackner-buckingham.com',
                                    subject: 'Finished Processing Story.',
                                    html: 'done'
                                };
                                Mailgun.messages().send(data, function (err, body) {
                                    res.send(pages);
                                });
                            });
                        }
                    }
                    next();
                });
            });
        });
    },

    del: function(req, res){
        res.send();
    },

    optimizeStory: function(req, res){
        Stories.findById(req.params.id).exec(function(err, story){
            var imgs = Object.create(story.images);
            var next = function(){
                if(imgs.length > 0){
                    var img = imgs.pop();
                    if(img != "FlyAwayHorse_IMAGE_PAGE_end.png"){
                        console.log('processing ' + img);
                        try{
                            var o = fs.createWriteStream('/var/www/client/app/assets/stories/' + img);
                            fs.createReadStream('/var/www/client/app/assets/test/' + img).pipe(new PngQuant()).pipe(o);
                            o.on('finish', function(){
                                next();
                            });
                        }catch(e){
                            next();
                        }
                    }else{
                        next();
                    }
                }else{
                    var exec = require('child_process').exec;
                    exec("rm /tmp/upload_*", function(error, stdout, stderr) {
                        exec("rm /var/www/client/app/assets/test/*", function(error, stdout, stderr) {
                            var data = {
                                from: 'no-reply@storysnap.org',
                                to: 'baird@lackner-buckingham.com',
                                subject: 'Finished Optimizing Story.',
                                html: 'done'
                            };
                            Mailgun.messages().send(data, function (err, body) {
                                res.send({});
                            });
                        });
                    });
                }
            }
            next();
        });
    },

    createThumb: function(req, res){
        Stories.findOne({_id: req.params.id}).exec(function(err, stories){
            var images = Object.create(stories.images);
            stories.thumbnail = images[0].split('?')[0].replace('.png', '-thumb.png');
            stories.save(function(err){
                var next = function(){
                    if(images.length > 0){
                        var story = images.pop();
                        var inname = story.split('?')[0];
                        var outname = inname.replace('.png', '-thumb.png')
                        var path = '/var/www/client/app/assets/stories/'
                        var imgin = path + inname
                        var imgout = path + outname
                        gm(imgin).resize(1024, 1024).write(imgout, function(){
                            next();
                        });
                    }else{
                        res.send({});
                    }
                }
                next();
            });
        });
    },

    createFeatured: function(req, res){
        Stories.findOne({_id: req.params.id}).exec(function(err, stories){
            var images = Object.create(stories.images);
            images = [images[0]];
            stories.featured = images[0].split('?')[0].replace('.png', '-featured.png');
            stories.save(function(err){
                var next = function(){
                    if(images.length > 0){
                        var story = images.pop();
                        var inname = story.split('?')[0];
                        var outname = inname.replace('.png', '-featured.png')
                        var path = '/var/www/client/app/assets/stories/'
                        var imgin = path + inname
                        var imgout = path + outname
                        gm(imgin).resize(1024, 1024).write(imgout, function(){
                            next();
                        });
                    }else{
                        res.send({});
                    }
                }
                next();
            })
        });
    }
};
