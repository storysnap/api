var fs = require('fs');
var sys = require('sys');
var Security = require('../lib/security');
var Config = require('../config/config');
var jwt    = require('jsonwebtoken');
var moment = require('moment');
var uuid = require('uuid');
var mongoose = require('mongoose');
var Users = mongoose.model('Users');
var Stories = mongoose.model('Stories');
var Ratings = mongoose.model('Ratings');
var _mailgun = require('mailgun-js');
var apn = require('apn');
var gm = require('gm');
require('string_score');
var natural = require('natural'),
    metaphone = natural.Metaphone, soundEx = natural.SoundEx;

var options = {
    production: true,
    cert: "/var/www/api/keys/aps_development.pem",
    key: "/var/www/api/keys/development.pem",
    passphrase: "T1meLo4d!"
};

var apnConnection = new apn.Connection(options);

apnConnection.on('error', function(err){
   console.log("error");
    console.log(err);
    //process.exit(0);
})

apnConnection.on('socketError', function(err){
    console.log("socket error");
    console.log(err);
    //process.exit(0);
})

apnConnection.on('transmitted', function (notification, device) {
    console.log("transmitted");
})

apnConnection.on('transmissionError', function(errorCode, notification, device) {
    console.log(device)
    console.log(errorCode);
})

apnConnection.on('completed', function(){
    console.log("done");
});

var internals = {};

var Mailgun = new _mailgun({apiKey: 'key-9dao0tj6p33aypwkko5ksg88s2g2wqs4', domain: 'mg.lackner-buckingham.com'});

var typeStruct = {
    storyToRecord: 0,
    invitationReceived: 1,
    invitationSent: 2,
    recordingByOthers: 3,
    recordingByMe: 4
}

var typeStateStruct = {
    storyToRecord: {
        recordOrResume: 0
    },
    invitationReceived: {
        acceptOrDecline: 0
    },
    invitationSent: {
        withdrawOrSendAgain: 0,
        remind: 1,
        recordYourselforSendNewInvite: 2,
        sendNewInvite:3
    },
    recordingByOthers: {
        review: 0,
        play: 1
    },
    recordingByMe: {
        playOrShare: 0
    }
}

exports = module.exports = internals.ProfessorsController = {

    purchase: function(req, res){
        var story = req.params.id;
        Users.findOne({_id:req.auth_id}, function(err, sender){
            if(sender){
                if(!sender.queue){
                    sender.queue = [];
                }

                if(!sender.receipts){
                    sender.receipts = [];
                }

                var exists = false;
                sender.receipts.forEach(function(rec){
                    if(rec.story == story){
                        exists = true;
                    }
                });

                if(!exists){
                    sender.receipts.push({story: story});
                    sender.activity.push({
                        action: "Purchased",
                        story: story
                    });
                    var uid = uuid.v1();
                    sender.queue.push({
                        story: story,
                        uid: uid,
                        user: sender._id,
                        type: typeStruct.storyToRecord,
                        type_state: typeStateStruct.storyToRecord.recordOrResume
                    });
                    sender.save(function(err, r){
                        var id = "";
                        r.queue.forEach(function(row){
                            if(row.uid == uid){
                                id = row._id;
                            }
                        })
                        res.send({_id: id});
                    });
                }else{
                    res.send({});
                }
            }else{
                res.send({});
            }
        });
    },

    library: function(req, res){
        Users.findOne({_id: req.auth_id, 'queue.archived': false}).populate('queue.story').populate('queue.user queue.shared_with.user circle', 'first_name last_name email profileImage title').exec(function(err, users){
            if(users){
                users = users || {};
                users.queue.sort(function(a,b){
                    a = new Date(a.date);
                    b = new Date(b.date);
                    return a>b ? -1 : a<b ? 1 : 0;
                });

                users.queue.filter(function(row){
                    if(row.archived != true && row.archived != 1){
                        return true;
                    }
                    return false;
                })
                res.send(users);
            }else{
                res.send({
                    queue: []
                });
            }
        });
    },

    circle: function(req, res){
        Users.findOne({_id: req.auth_id}).populate('circle', 'first_name last_name email profileImage title').exec(function(err, users){
            if(users){
                res.send(users.circle || []);
            }else{
                res.send([]);
            }
        });
    },

    purchases: function(req, res){
        Users.findOne({_id: req.auth_id}).populate('receipts.story').exec(function(err, users){
            res.send(users);
        });
    },

    purchasesById: function(req, res){
        Users.findOne({_id: req.auth_id}).exec(function(err, users){
            try{
                var ex = 0;
                if(users.receipts){
                    users.receipts.forEach(function(story){
                        if(story.story == req.params.id){
                            ex = 1
                        }
                    })
                }

                res.send({exists: ex});
            }catch(e){
                console.log(e);
                res.send({exists: 0});
            }
        });
    },

    favorite: function(req, res){
        var story = req.params.id;
        Users.findOne({_id:req.auth_id}, function(err, sender){
            if(sender){
                if(!sender.favorites){
                    sender.favorites = [];
                }
                var exists = false;
                sender.favorites.forEach(function(rec){
                    if(rec == story){
                        exists = true;
                    }
                });
                if(!exists){
                    sender.activity.push({
                        action: "Favorited",
                        story: story
                    });
                    sender.favorites.push(story);
                    sender.save(function(err, r){
                        res.send({});
                    });
                }else{
                    res.send({});
                }
            }else{
                res.send({});
            }
        });
    },

    removeFavorite: function(req, res){
        var story = req.params.id;
        Users.findOne({_id:req.auth_id}, function(err, sender){
            if(sender){
                if(!sender.favorites){
                    sender.favorites = [];
                }

                sender.favorites = sender.favorites.filter(function(rec){
                    if(rec == story){
                        return false;
                    }
                    return true;
                });
                sender.activity.push({
                    action: "Removed Favorite",
                    story: story
                });
               sender.save(function(err, r){
                   console.log(err);
                   console.log(r);
                    res.send({msg:"success", id: story});
                });
            }else{
                res.send({});
            }
        });
    },

    archive: function(req, res){
        Users.findOne({_id: req.auth_id}).exec(function(err, user){
            if(!user.queue){
                user.queue = [];
            }

            var idx = -1;
            user.queue.forEach(function(story, ix){
                if(story._id == req.params.id){
                    idx = ix;
                }
            });

            if(idx != -1){
                user.queue[idx].archived = true;
            }

            user.save(function(err, usr){
                res.send({err: err, user: usr});
            })
        });
    },

    saveRecording: function(req, res){
        var key = Object.keys(req.files).pop();
        var file = req.files[key];
        var dir = '/var/www/client/app/assets/audio/' + req.auth_id + '/';
        var source = fs.createReadStream(file.path);
        var fname = file.name;
        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir);
        }
        var dest = fs.createWriteStream(dir + fname);

        source.pipe(dest);
        source.on('end', function() {
            fs.unlink(file.path, function(){
                var k = key.replace('.m4a', '');
                Users.findOne({_id: req.auth_id}).exec(function(err, user){

                    if(!user.queue){
                        user.queue = [];
                    }

                    var idx = -1;
                    user.queue.forEach(function(row, ix){
                        if(row._id == req.params.id){
                            idx = ix;
                        }
                    });

                    if(idx != -1){
                        user.queue[idx].breaks = JSON.parse(req.headers["x-page-breaks"]);
                        user.queue[idx].file = req.auth_id + '/' + fname;
                        user.queue[idx].completed = true;
                        user.queue[idx].times_shared = 0;
                        user.queue[idx].date = Date.now()
                        user.queue[idx].type = typeStruct.recordingByMe
                        user.activity.push({
                            action: "Finished Recording",
                            story: user.queue[idx].story
                        });

                        if(user.queue[idx].accepted){
                            user.queue[idx].times_shared = 1;
                            Users.findOne({_id: user.queue[idx].user}).exec(function(err, rec){
                                rec.queue.forEach(function(row, ix){
                                    if(row.uid == user.queue[idx].uid){
                                        row.breaks = user.queue[idx].breaks;
                                        row.user = req.auth_id;
                                        row.file = req.auth_id + '/' + fname;
                                        row.date = Date.now();
                                        row.completed = true;
                                        row.type = typeStruct.recordingByOthers;
                                        row.type_state = typeStateStruct.recordingByOthers.play;
                                    }
                                });
                                rec.save(function(err, u){
                                   user.save(function(err, usr){
                                        res.send({err: err, user: usr});
                                    });
                                });
                            });
                        }else{
                            user.save(function(err, usr){
                                res.send({err: err, user: usr});
                            });
                        }
                    }else{
                        console.log('something went wrong');
                        res.send({err: "error"});
                    }

                });
            });
        });
        source.on('error', function(err) {
            res.send({msg: 'error'});
        });
    },

    shareRecording: function(req, res){
        var story = req.params.id;

        var send = function(sender, rec, newUser){
            if(!rec.queue){
                rec.queue = [];
            }

            if(!sender.queue){
                sender.queue = [];
            }

            if(!rec.circle){
                rec.circle = [];
            }

            if(!sender.circle){
                sender.circle = [];
            }

            var allowedToShare = true;
            var alreadySharedWithUser = false;
            var sameUser = false;
            var indx = 0;
            var i = -1;
            sender.queue.forEach(function(r, idx){
                if(r._id == story){
                    i = idx;
                }
            });

            var times_shared = sender.queue[i].shared_with.length;

            if(sender._id == rec._id){
                allowedToShare = false;
                sameUser = true;
            }

            sender.queue[i].shared_with.forEach(function(row){
                console.log(row.user + ' - ' + rec._id);
                console.log(row.user.toString().trim().toLowerCase() == rec._id.toString().trim().toLowerCase());
                console.log('-------');
                if(row.user.toString().trim().toLowerCase() == rec._id.toString().trim().toLowerCase()){
                    console.log('matched');
                    allowedToShare = false;
                    alreadySharedWithUser = true;
                }
            });

            if(times_shared >= 3){
                allowedToShare = false;
            }

            console.log(allowedToShare);
            console.log('----')

            if(allowedToShare){

                if(rec.circle.indexOf(sender._id) == -1){
                    rec.circle.push(sender._id);
                }

                if(sender.circle.indexOf(rec._id) == -1){
                    sender.circle.push(rec._id);
                }

                var msg = "";
                var subject = sender.first_name + " " + sender.last_name + " has recorded the story " + req.body.storyName + " for you"
                if(newUser){
                    msg = "<p>Greetings,</p>";
                    if(req.body.message != ""){
                        msg += '<p style="background:#eeeeee; padding:5px;">' + decodeURIComponent(req.body.message) + "</p>";
                    }

                    msg += "<p>To listen to the story " + req.body.storyName + " recorded for you by " + sender.first_name + " " + sender.last_name + " using the StorySnap app, please <a href=\"#\">click here</a> to install the StorySnap app on your iPad or iPhone."
                }else{
                    msg = "<p>Dear " + rec.first_name + ",</p>";
                    if(req.body.message != ""){
                        msg += '<p style="background:#eeeeee; padding:5px;">' + decodeURIComponent(req.body.message) + '</p>';
                    }

                    msg += "<p>To review the story " + req.body.storyName + " recorded for you by " + sender.first_name + " " + sender.last_name + " using the StorySnap app, please go to the manage stories section."
                }

                msg += "<p>If you have any technical questions, please email: contact@storysnap.org</p>";

                // var msg = "<p>You have been invited to view a story on Story Snap.</p>";
                // if(req.body.message != ""){
                //     msg = "You have a been invited to view a story on Story Snap by " + req.body.name;
                //     msg += "<p>" + req.body.message + "</p>";
                // }
                var data = {
                    from: 'no-reply@storysnap.org',
                    to: req.body.email,
                    subject: 'A story has been shared with you on Story Snap',
                    html: msg
                };

               var s = {
                    story: sender.queue[i].story,
                    user: sender._id,
                    file: sender.queue[i].file,
                    breaks: sender.queue[i].breaks,
                    message: req.body.message || "",
                    fromName: req.body.knownName || "",
                    dedication: req.body.dedication ? sender._id + '/' + req.body.dedication : "",
                    type: typeStruct.recordingByOthers,
                    type_state: typeStateStruct.recordingByOthers.review
                };

                rec.queue.push(s);

                sender.queue[i].shared_with.push({
                    user: rec._id
                });
                sender.queue[i].times_shared = sender.queue[i].times_shared + 1;
                sender.activity.push({
                    action: "Shared Recording With " + req.body.email,
                    story: story
                });

                rec.activity.push({
                    action: "Received Recording from " + sender.email,
                    story: story
                });

                rec.save(function(err, r){
                    sender.save(function(err2, r2){
                        try{
                            if(rec.deviceTokens){
                                var tokens = rec.deviceTokens;
                                var note = new apn.Notification();
                                note.badge = 1;
                                note.sound = "ping.aiff";
                                note.alert = sender.first_name + ' ' + sender.last_name + " has shared a story with you.";
                                note.payload = {'messageFrom': 'StorySnap'};
                                apnConnection.pushNotification(note, tokens);
                            }
                        }catch(e){
                            console.log(e);
                        }

                        Mailgun.messages().send(data, function (err, body) {
                            res.send({});
                        });
                    });
                });
            }else{
                console.log('error');
                sender.save(function(err2, r2){
                    if(alreadySharedWithUser){
                        res.send({msg:"You have already shared this story with this user."});
                    }else if(sameUser){
                        res.send({msg:"You cannot share story with yourself."});
                    }else{
                        sender.queue[i].times_shared = 3;
                        res.send({msg:"You can only share a story 3 times."});
                    }
                });
            }
        }

        Users.findOne({email:req.body.email.toLowerCase()}, function(err, rec){
            Users.findOne({_id:req.auth_id}, function(err, sender){
                if(rec && sender){
                    send(sender, rec, false);
                }else{
                    var n = req.body.name.split(' ');
                    var first_name = n[0];
                    var last_name = n[1];
                    var model = new Users();
                    var n = moment();
                    model.email = req.body.email.toLowerCase();
                    model.first_name = first_name;
                    model.last_name = last_name;
                    model.active = false;
                    model.password = uuid.v1();
                    model.created = moment().format('x');
                    model.externalUser = true;
                    model.queue = [];
                    send(sender, model, true);
                }
            });
        });
    },

    kids: function(req, res){
        Users.findOne({_id: req.auth_id, 'queue.archived': false}).populate('queue.story').populate('queue.user queue.shared_with.user', 'first_name last_name email profileImage title').exec(function(err, users){
            users = users || {queue: []};
            users.queue.sort(function(a,b){
                a = new Date(a.date);
                b = new Date(b.date);
                return a>b ? -1 : a<b ? 1 : 0;
            });

            var all = users.queue.filter(function(row){
                if(row.file && !row.archived && row.archived != 1){
                    if(row.type == typeStruct.recordingByOthers && row.type_state == typeStateStruct.recordingByOthers.review){
                        return false;
                    }else{
                        return true;
                    }
                }
                return false;
            })
            res.send(all);
        });
    },

    create: function(req, res){
        console.log(req.body);
        Users.findOne({email:req.body.email.toLowerCase()}, function(err, users){
            if(!users){
                var security = new Security(req.body.password);
                security.hash().then(function(){
                    var model = new Users(req.body);
                    var n = moment();
                    model.email = model.email.toLowerCase()
                    model.password = security.hash;
                    model.created = moment().format('x');
                    try{
                    if(!model.queue){
                        model.queue = [];
                    }

                    model.queue.push({
            			"story" : "5900f2b08c3103b538f5375d",
            			"user" : "5902328cb0d59f93513fe97b",
            			"times_recorded" : 0,
            			"archived" : false,
            			"viewed" : false,
            			"needToPurchase" : false,
            			"withdrawn" : false,
            			"declined" : false,
            			"completed" : true,
            			"accepted" : false,
            			"fromName" : "",
            			"dedication" : "",
            			"uid" : "ecbc8260-2b73-11e7-9c22-cfe85da29633",
            			"message" : "",
            			"readerName" : "",
                        "date": new Date('01/01/2000'),
            			"type_state" : 1,
                        "type" : 3,
            			"shared_with" : [ ],
            			"times_shared" : 0,
            			"file" : "5902328cb0d59f93513fe97b/5902329fb0d59f93513fe97e.m4a",
            			"breaks" : [
            				0,
            				4.899410430839002,
            				25.63482993197279,
            				39.68927437641723,
            				59.21725623582766,
            				72.40619047619047,
            				93.58278911564626,
            				109.5813378684807,
            				125.8120861678005,
            				139.4421995464853,
            				162.9640136054422,
            				178.4517233560091,
            				204.2490929705215
            			]
            		});
                    }catch(e){
                        console.log(e);
                    }

                    model.save(function(err, users){
                        var str = JSON.stringify({id: model._id, timestamp:moment().format('x')});
                        var token = jwt.sign(str, Config.secret, {});
                        var data = {
                            from: 'no-reply@storysnap.org',
                            to: req.body.email,
                            subject: 'Story Snap Account Confirmation',
                            html: '<p>Click the link below to activate your account.</p><p><a href="http://104.130.17.200/account/activate/' + token + '">Activate</a></p><p>If you have any technical questions, please email: contact@storysnap.org</p>'
                        };

                        Mailgun.messages().send(data, function (err, body) {
                            res.send({token:token, email:req.body.email});
                        });
                    });
                }, function(){
                    res.send({token:'', email:''});
                });
            }else if(!users.active){
               var security = new Security(req.body.password);
                security.hash().then(function(){
                    users.password = security.hash;
                    users.created = moment().format('x');
                    users.active = true;

                    if(!users.queue){
                        users.queue = [];
                    }

                    users.queue.push({
            			"story" : "5900f2b08c3103b538f5375d",
            			"user" : "5902328cb0d59f93513fe97b",
            			"times_recorded" : 0,
            			"archived" : false,
            			"viewed" : false,
            			"needToPurchase" : false,
            			"withdrawn" : false,
            			"declined" : false,
            			"completed" : true,
            			"accepted" : false,
            			"fromName" : "",
            			"dedication" : "",
            			"uid" : "ecbc8260-2b73-11e7-9c22-cfe85da29633",
            			"message" : "",
            			"readerName" : "",
                        "date": new Date('01/01/2000'),
            			"type_state" : 1,
                        "type" : 3,
            			"shared_with" : [ ],
            			"times_shared" : 0,
            			"file" : "5902328cb0d59f93513fe97b/5902329fb0d59f93513fe97e.m4a",
            			"breaks" : [
            				0,
            				4.899410430839002,
            				25.63482993197279,
            				39.68927437641723,
            				59.21725623582766,
            				72.40619047619047,
            				93.58278911564626,
            				109.5813378684807,
            				125.8120861678005,
            				139.4421995464853,
            				162.9640136054422,
            				178.4517233560091,
            				204.2490929705215
            			]
            		});

                    req.body.password = users.password
                    users.save(function(err, users){
                        Users.update({_id: users._id}, req.body, function(err, data){
                            var str = JSON.stringify({id: users._id, timestamp:moment().format('x')});
                            var token = jwt.sign(str, Config.secret, {});
                            var data = {
                                from: 'no-reply@storysnap.org',
                                to: req.body.email,
                                subject: 'Story Snap Account Confirmation',
                                html: '<p>Click the link below to activate your account.</p><p><a href="http://104.130.17.200/account/activate/' + token + '">Activate</a></p><p>If you have any technical questions, please email: contact@storysnap.org</p>'
                            };

                            Mailgun.messages().send(data, function (err, body) {
                                res.send({token:token, email:req.body.email});
                            });
                        });
                    });
                }, function(){
                    res.send({token:'', email:''});
                });
            }else{
                res.send({token:'', email:''});
            }
        });
    },

    inviteUser: function(req, res){
        var story = req.params.id;
        var send = function(sender, rec, newUser){
            var msg = "";
            var subject = sender.first_name + " " + sender.last_name + " has invited you to narrate the story " + req.body.storyName
            if(newUser){
                msg = "<p>Greetings,</p>";
                if(req.body.message != ""){
                    msg += '<p style="background:#eeeeee; padding:5px;">' + decodeURIComponent(req.body.message) + "</p>";
                }

                msg += "<p>Read a story to someone you love, from wherever you are in the world. Create personally narrated ebooks to treasure forever using the StorySnap app.</p>"
                msg += "<p>To respond to the invitation from " + sender.first_name + " " + sender.last_name + " to record the story " + req.body.storyName + " using the StorySnap app, please <a href=\"#\">click here</a> to install the StorySnap app on your iPad or iPhone at no cost. You can preview a few pages of the story before deciding to accept or decline the invitation.</p>"
            }else{
                msg = "<p>Dear " + rec.first_name + ",</p>";
                if(req.body.message != ""){
                    msg += '<p style="background:#eeeeee; padding:5px;">' + decodeURIComponent(req.body.message) + "</p>";
                }

                msg += "<p>Read a story to someone you love, from wherever you are in the world. Create personally narrated ebooks to treasure forever using the StorySnap app."
                msg += "<p>To respond to the invitation from " + sender.first_name + " " + sender.last_name + " to record the story " + req.body.storyName + " using the StorySnap app, please go to the manage stories section. You can preview a few pages of the story before deciding to accept or decline the invitation."
            }

            msg += "<p>If you have any technical questions, please email: contact@storysnap.org</p>";

            var data = {
                from: 'no-reply@storysnap.org',
                to: req.body.email,
                subject: subject,
                html: msg
            };

            if(!rec.queue){
                rec.queue = [];
            }

            if(!sender.queue){
                sender.queue = [];
            }

            if(!rec.circle){
                rec.circle = [];
            }

            if(!sender.circle){
                sender.circle = [];
            }

            if(rec.circle.indexOf(sender._id) == -1){
                rec.circle.push(sender._id);
            }

            if(sender.circle.indexOf(rec._id) == -1){
                sender.circle.push(rec._id);
            }

            var unique_id = uuid.v1();

            var inviteReceived = {
                story: story,
                user: sender._id,
                needToPurchase: req.body.purchasing == "yes" ? false : true,
                message: req.body.message,
                completed: false,
                accepted: false,
                declined: false,
                uid: unique_id,
                type: typeStruct.invitationReceived,
                type_state: typeStateStruct.invitationReceived.acceptOrDecline,
                fromName: req.body.knownName || "",
                shared_with: [{
                    user: sender._id
                }]
            };

            var inviteSent = {
                story: story,
                user: rec._id,
                needToPurchase: req.body.purchasing == "yes" ? false : true,
                completed: false,
                accepted: false,
                declined: false,
                uid: unique_id,
                type: typeStruct.invitationSent,
                type_state: typeStateStruct.invitationSent.withdrawOrSendAgain
            };

            if(!sender.receipts){
                sender.receipts = [];
            }

            if(req.body.purchasing == "yes"){
                sender.receipts.push({
                    story: story
                })
            }

            sender.activity.push({
                action: "Invited " + req.body.name + " to read story",
                story: story
            });

            rec.activity.push({
                action: "Received invitation from " + req.body.knownName,
                story: story
            });

            rec.queue.push(inviteReceived);
            sender.queue.push(inviteSent);
            rec.save(function(err, r){
                sender.save(function(err, r){
                    if(rec.deviceTokens){
                        var tokens = rec.deviceTokens
                        var note = new apn.Notification();
                        note.badge = 1;
                        note.sound = "ping.aiff";
                        note.alert = sender.first_name + ' ' + sender.last_name + " has invited you to read a story.";
                        note.payload = {'messageFrom': 'StorySnap'};
                        apnConnection.pushNotification(note, tokens);
                    }
                    Mailgun.messages().send(data, function (err, body) {
                        console.log(err);
                        res.send({});
                    });
                });
            });
        }
        Users.findOne({email:req.body.email.toLowerCase()}, function(err, rec){
            Users.findOne({_id:req.auth_id}, function(err, sender){
                if(rec && sender){
                    send(sender, rec, false);
                }else{
                    console.log('creating new');
                    var n = req.body.name.split(' ');
                    var first_name = n[0];
                    var last_name = n[1];
                    var model = new Users();
                    var n = moment();
                    model.email = req.body.email.toLowerCase();
                    model.first_name = first_name || "";
                    model.last_name = last_name || "";
                    model.active = false;
                    model.password = uuid.v1();
                    model.created = moment().format('x');
                    model.externalUser = true;

                    send(sender, model, true);
                }
            });
        });
    },

    inviteDifferentUser: function(req, res){
        var story = req.params.id;
        var send = function(sender, rec, newUser){
            var msg = "";
            var subject = sender.first_name + " " + sender.last_name + " has invited you to narrate the story " + req.body.storyName
            if(newUser){
                msg = "<p>Greetings,</p>";
                if(req.body.message != ""){
                    msg += '<p style="background:#eeeeee; padding:5px;">' + decodeURIComponent(req.body.message) + "</p>";
                }

                msg += "<p>Read a story to someone you love, from wherever you are in the world. Create personally narrated ebooks to treasure forever using the StorySnap app.</p>"
                msg += "<p>To respond to the invitation from " + sender.first_name + " " + sender.last_name + " to record the story " + req.body.storyName + " using the StorySnap app, please <a href=\"#\">click here</a> to install the StorySnap app on your iPad or iPhone at no cost. You can preview a few pages of the story before deciding to accept or decline the invitation.</p>"
            }else{
                msg = "<p>Dear " + rec.first_name + ",</p>";
                if(req.body.message != ""){
                    msg += '<p style="background:#eeeeee; padding:5px;">' + decodeURIComponent(req.body.message) + "</p>";
                }

                msg += "<p>Read a story to someone you love, from wherever you are in the world. Create personally narrated ebooks to treasure forever using the StorySnap app."
                msg += "<p>To respond to the invitation from " + sender.first_name + " " + sender.last_name + " to record the story " + req.body.storyName + " using the StorySnap app, please go to the manage stories section. You can preview a few pages of the story before deciding to accept or decline the invitation."
            }

            msg += "<p>If you have any technical questions, please email: contact@storysnap.org</p>";

            var data = {
                from: 'no-reply@storysnap.org',
                to: req.body.email,
                subject: subject,
                html: msg
            };

            if(!rec.queue){
                rec.queue = [];
            }

            if(!sender.queue){
                sender.queue = [];
            }

            if(!rec.circle){
                rec.circle = [];
            }

            if(!sender.circle){
                sender.circle = [];
            }

            if(rec.circle.indexOf(sender._id) == -1){
                rec.circle.push(sender._id);
            }

            if(sender.circle.indexOf(rec._id) == -1){
                sender.circle.push(rec._id);
            }

            var unique_id = req.body.uid;

            sender.queue.forEach(function(row){
                if(row.uid == unique_id){
                    row.user = rec._id;
                    row.date = Date.now();
                    row["type"] = 2;
                    row.type_state = 0;
                    row.message = req.body.message;
                    row.declined = false;
                    row.user = rec._id;
                    row.needToPurchase = req.body.purchasing == "yes" ? false : true;
                }
            })

            var inviteReceived = {
                story: story,
                user: sender._id,
                needToPurchase: req.body.purchasing == "yes" ? false : true,
                message: req.body.message,
                completed: false,
                accepted: false,
                declined: false,
                uid: unique_id,
                type: typeStruct.invitationReceived,
                type_state: typeStateStruct.invitationReceived.acceptOrDecline,
                fromName: req.body.knownName || "",
                shared_with: [{
                    user: sender._id
                }]
            };

            sender.activity.push({
                action: "Invited " + req.body.name + " to read story",
                story: story
            });

            rec.activity.push({
                action: "Received invitation from " + req.body.knownName,
                story: story
            });

            rec.queue.push(inviteReceived);
            rec.save(function(err, r){
                sender.save(function(err, r){
                    if(rec.deviceTokens){
                        var tokens = rec.deviceTokens
                        var note = new apn.Notification();
                        note.badge = 1;
                        note.sound = "ping.aiff";
                        note.alert = sender.first_name + ' ' + sender.last_name + " has invited you to read a story.";
                        note.payload = {'messageFrom': 'StorySnap'};
                        apnConnection.pushNotification(note, tokens);
                    }
                    Mailgun.messages().send(data, function (err, body) {
                        console.log(err);
                        res.send({});
                    });
                });
            });
        }
        Users.findOne({email:req.body.email.toLowerCase()}, function(err, rec){
            Users.findOne({_id:req.auth_id}, function(err, sender){
                if(rec && sender){
                    send(sender, rec, false);
                }else{
                    console.log('creating new');
                    var n = req.body.name.split(' ');
                    var first_name = n[0];
                    var last_name = n[1];
                    var model = new Users();
                    var n = moment();
                    model.email = req.body.email.toLowerCase();
                    model.first_name = first_name || "";
                    model.last_name = last_name || "";
                    model.active = false;
                    model.password = uuid.v1();
                    model.created = moment().format('x');
                    model.externalUser = true;

                    send(sender, model, true);
                }
            });
        });
    },

    remindInvite: function(req, res){
        var uid = req.body.uid;
        Users.findOne({_id:req.body.user}).populate('queue.user', 'first_name last_name email profileImage title').exec(function(err, rec){
            Users.findOne({_id:req.auth_id}).populate('queue.user', 'first_name last_name email profileImage title').exec(function(err, sender){

                var data = {
                    from: 'no-reply@storysnap.org',
                    to: rec.email,
                    subject: 'Story Snap Reminder',
                    html: sender.first_name + ' ' + sender.last_name + ' has sent you a reminder to please record the story ' + req.body.title + ' using the StorySnap app'
                };

                Mailgun.messages().send(data, function (err, body) {
                    console.log(err);
                    res.send({});
                });

                res.send({msg: "done"})
            });
        });
    },

    revokeInvite: function(req, res){
        var uid = req.body._id;
        Users.findOne({_id:req.body.user}).exec(function(err, rec){
            Users.findOne({_id:req.auth_id}).exec(function(err, sender){
                var _uid = ""
                var story = "";
                sender.queue.forEach(function(row){
                    if(row._id == uid){
                        _uid = row.uid;
                        story = row.story;
                        row.date = Date.now();
                        row.declined = false;
                        row.withdrawn = true;
                        row.user = req.auth_id;
                        row.type = typeStruct.invitationSent;
                        if(row.needToPurchase){
                            row.type_state = typeStateStruct.invitationSent.sendNewInvite;
                        }else{
                            row.type_state = typeStateStruct.invitationSent.recordYourselforSendNewInvite;
                        }
                    }
                });

                rec.queue = rec.queue.filter(function(row){
                    if(row.uid == _uid){
                        return false;
                    }
                    return true;
                });

                sender.activity.push({
                    action: "Revoked Invite to " + rec.first_name + " " + rec.last_name + " to read story",
                    story: story
                });

                rec.save(function(){
                    sender.save(function(){
                        res.send({msg: "done"})
                    })
                })
            });
        });
    },

    acceptInvite: function(req, res){
        var uid = req.body.uid;
        Users.findOne({_id:req.auth_id}).exec(function(err, rec){
            Users.findOne({_id:req.body.user}).exec(function(err, sender){
                var story = ""
                sender.queue.forEach(function(row){
                    if(row.uid == uid){
                        story = row.story;
                        row.date = Date.now();
                        row.accepted = true;
                        row.type_state = typeStateStruct.invitationSent.remind
                    }
                });

                rec.queue.forEach(function(row){
                    if(row.uid == uid){
                        row.date = Date.now();
                        row.accepted = true;
                        row.type = typeStruct.storyToRecord
                        row.type_state = typeStateStruct.storyToRecord.recordOrResume
                    }
                });

                rec.activity.push({
                    action: "Accepted invite from " + sender.email,
                    story: story
                });

                rec.save(function(){
                    sender.save(function(){
                        res.send({msg: "done"})
                    })
                })
            });
        });
    },

    declineInvite: function(req, res){
        var uid = req.body.uid;
        Users.findOne({_id:req.auth_id}).exec(function(err, rec){
            Users.findOne({_id:req.body.user}).exec(function(err, sender){
                var story = "";
                sender.queue.forEach(function(row){
                    if(row.uid == uid){
                        story = row.story;
                        row.date = Date.now();
                        row.declined = true;
                        row.user = sender._id;
                        row.type = typeStruct.invitationSent;
                        if(row.needToPurchase){
                            row.type_state = typeStateStruct.invitationSent.sendNewInvite;
                        }else{
                            row.type_state = typeStateStruct.invitationSent.recordYourselforSendNewInvite;
                        }
                    }
                });

                rec.queue = rec.queue.filter(function(row){
                    if(row.uid == uid){
                        return false;
                    }
                    return true;
                });

                sender.activity.push({
                    action: rec.first_name + " " + sender.last_name + " declined invite to read story",
                    story: story
                });

                rec.activity.push({
                    action: "Declined Invite from " + sender.first_name + " " + sender.last_name + " to read story",
                    story: story
                });

                rec.save(function(){
                    sender.save(function(){
                        res.send({msg: "done"})
                    })
                })
            });
        });
    },

    acceptShare: function(req, res){
        Users.findOne({_id:req.auth_id}).exec(function(err, user){

            user.queue.forEach(function(row){
                if(row._id == req.params.id){
                    row.accepted = true;
                    row.type = typeStruct.recordingByOthers
                    row.type_state = typeStateStruct.recordingByOthers.play
                }
            });

            user.save(function(){
                res.send({msg: "done"})
            })
        });
    },

    sendPin: function(req, res){
        Users.findOne({_id: req.auth_id}).select('+pin').exec(function(err, user){
            if(user){
                var data = {
                    from: 'no-reply@storysnap.org',
                    to: user.email,
                    subject: 'Story Snap Pin Code Recovery',
                    html: '<p>Here is your pin code ' + (user.pin || '0000') +'. To change your pin code at any time, go to Main Menu > Settings > Preferences.</p><p>If you have any technical questions, please email: contact@storysnap.org</p>'
                };

                Mailgun.messages().send(data, function (err, body) {
                    res.send();
                });
            }else{
                res.send();
            }
        });
    },

    launchedStory: function(req, res){
        Users.findOne({_id:req.auth_id}).exec(function(err, user){

            user.queue.forEach(function(row){
                if(row._id == req.params.id){
                    row.times_recorded += 1;
                }
            });

            user.save(function(){
                res.send({msg: "done"})
            })
        });
    },

    previewStory: function(req, res){
        Users.findOne({_id:req.auth_id}).exec(function(err, user){
			if(user){
	            if(!user.previews){
	                user.previews = [];
	            }

	            if(user.previews.indexOf(req.params.id) == -1){
	                user.previews.push(req.params.id);
	            }

	            user.save(function(){
	                Stories.findOne({_id:req.params.id}).exec(function(err, story){
	                    story.previewed += 1;
	                    story.save(function(){
	                        res.send({msg: "done"})
	                    })
	                });
	            });
			}else{
				res.send({msg: "done"})
			}
        });
    },

    launchedApp: function(req, res){
        Users.findOne({_id: req.auth_id}).exec(function(err, user){
			if(user){
	            user.numberOfSessions += 1;
	            user.lastActive = Date.now();
	            user.save(function(){
	                res.send({});
	            });
			}else{
				res.send({});
			}
        });
    },

    closedApp: function(req, res){
        Users.findOne({_id: req.auth_id}).exec(function(err, user){
			if(user){
	            user.numberOfSessions += 1;
	            var n = moment();
	            var sessionStarted = moment(user.lastActive);
	            var days = n.diff(sessionStarted, 'days');
	            var time = n.diff(sessionStarted, 'seconds');
	            if(days > 1){
	                time = 0;
	            }
	            user.totalTime += time;
	            user.save(function(){
	                res.send({});
	            });
			}else{
				res.send({});
			}
        });
    },

    search: function(req, res){
        Users.find({}, function(err, user){
            var keyword = req.querystring.keyword;
            var users = [];
            var num_matches = 0;
            user.forEach(function(u){
                var score = 0;
                var k = keyword.split(/[ -]/g);
                var matches = 0;
                k.forEach(function(key){
                    if(u.first_name != "" && u.last_name != "" && u._id != req.auth_id){
                        var local_score = 0;
                        var fn = u.first_name.toLowerCase().score(key.toLowerCase());
                        var ln = u.last_name.toLowerCase().score(key.toLowerCase());
                        var em = u.email.split('@')[0].toLowerCase().score(key.toLowerCase());
                        var em2 = 0;//u.email.score(key);
                        local_score += fn + ln + em + em2;
                        var first_match = metaphone.compare(u.first_name.toLowerCase(), key.toLowerCase());
                        var last_match = metaphone.compare(u.last_name.toLowerCase(), key.toLowerCase());
                        var first_match2 = soundEx.compare(u.first_name.toLowerCase(), key.toLowerCase());
                        var last_match2 = soundEx.compare(u.last_name.toLowerCase(), key.toLowerCase());
                        if(first_match){
                            local_score += 0.25;
                            matches += 1;
                        }

                        if(first_match2){
                            local_score += 0.25;
                            matches += 1;
                        }

                        if(last_match){
                            local_score += 0.25;
                            matches += 1;
                        }

                        if(last_match2){
                            local_score += 0.25;
                            matches += 1;
                        }
                    }
                    score += local_score
                })
                if(score > 0){
                    users.push({
                        _id: u._id,
                        profileImage: u.profileImage,
                        email: u.email,
                        name: u.first_name + ' ' + u.last_name,
                        score: score + (matches * 0.25),
                        matches: matches
                    })
                }
            })
            users = users.sort(function(a,b){
                if(a.score < b.score) return 1;
                if(a.score > b.score) return -1;
                return 0;
            })
            res.send(users);
        });
    },

    isVerified: function(req, res){
        Users.findOne({_id:req.auth_id}).select('+pin').exec(function(err, user){
            if(user){
                res.send({confirmed: user.confirmed, pin: user.pin || "", _id: user._id, email: user.email});
            }else{
                res.send({confirmed: -1, pin: "", _id: ""});
            }
        });
    },

    verify: function(req, res){
        Security.verify(req.params.id).then(function(user){
            Users.findOne({_id:user.id}, function(err, user){
                user.dateActived = Date.now();
                user.confirmed = true;
                user.save(function(err, user){
                    res.send(user);
                })
            });
        });
    },

    login: function(req, res){
        try{
            if(req.body.email !== '' && req.body.password !== ''){
                var security = new Security(req.body.password);
                Users.findOne({email:req.body.email}).select('+password').exec(function(err, users){
                    if(err){
                        console.log(err);
                    }
                    if(users){
                        security.compare(users.password).then(function(isMatch){
                            if(isMatch){
                                var str = JSON.stringify({id: users._id, timestamp:moment().format('x')});
                                var token = jwt.sign(str, Config.secret, {});
                                res.send({token:token, email:req.body.email, pin: users.pin});
                            }else{
                                res.status = 401;
                                res.send({token:'', email:'', msg:'Invalid username or password.'});
                            }
                        }, function(err){
                            res.status = 401;
                            res.send({token:'', email:'', msg:'Invalid username or password.'});
                        });
                    }else{
                        res.status = 401;
                        res.send({token:'', email:'', msg:'Invalid username or password.'});
                    }
                });
            }else{
                res.send({token:'', email:'', msg:'Invalid username or password.'});
            }
        }catch(e){
            res.send({token:'', email:'', msg:'Invalid username or password.'});
        }
    },

    logout: function(req, res){
        res.send({msg: 'logged out'});
    },

    sendRecover: function(req, res){
        Users.findOne({email: req.body.email}, function(err, user){
            if(user){
                var token = uuid.v1();
                user.recoverToken = token;
                user.save(function(){
                    var html = '<p>Follow the link below to reset your password.</p>';
                    html += '<p><a href="http://104.130.17.200/users/reset/' + token + '">Reset Password</a></p><p>If you have any technical questions, please email: contact@storysnap.org</p>';
                    var data = {
                        from: 'no-reply@storysnap.org',
                        to: req.body.email,
                        subject: 'Story Snap Password Reset',
                        html: html
                    };

                    Mailgun.messages().send(data, function (err, body) {
						console.log(err);
						console.log(body);
                        res.send({});
                    });
                })
            }else{
                res.send();
            }
        });
    },

    reset: function(req, res){
        if(req.params.token){
            Users.findOne({recoverToken: req.params.token}, function(err, user){
                if(user){
                    var security = new Security(req.body.password);
                    security.hash().then(function(){
                        user.recoverToken = '';
                        user.password = security.hash;
                        user.save(function(err, model){
                            var str = JSON.stringify({id: model._id, timestamp:moment().format('x')});
                            var token = jwt.sign(str, Config.secret, {});
                            res.send({token:token});
                        });
                    })
                }else{
                    res.send({msg:"Error Resetting password"});
                }
            });
        }else{
            res.send({msg:"Error Resetting password"});
        }
    },

    profile: function(req, res){
        Users.findOne({_id: req.auth_id}).select('first_name last_name email title country languages birth_year favorite_books favorite_books_to_read profileImage pin').exec(function(err, users){
            users = users || {};
            res.send(users);
        });
    },

    recordings: function(req, res){
        Users.findOne({_id: req.auth_id}).exec(function(err, users){
            users = users || [];
            res.send(users);
        });
    },

    invitations: function(req, res){
        Users.findOne({_id: req.auth_id}).populate('invitation.story').populate('invitation.user').exec(function(err, users){
            users = users || [];
            res.send(users);
        });
    },

    shared: function(req, res){
        Users.findOne({_id: req.auth_id}).populate('shared.story').populate('shared.user').exec(function(err, users){
            users = users || [];
            res.send(users);
        });
    },

    find: function(req, res){
        Users.findOne({_id: req.auth_id}).exec(function(err, users){
            users = users || [];
            res.send(users);
        });
    },

    setDevice: function(req, res){
        Users.findById(req.auth_id).exec(function(err, user){
            if(user){
                if(!user.deviceTokens){
                    user.deviceTokens = [];
                }
                var idx = user.deviceTokens.indexOf(req.body.token);
                if(idx == -1){
                    user.deviceTokens.push(req.body.token);
                }
                user.deviceTokens = user.deviceTokens.filter(function(token){
                    if(token != null){
                        return true;
                    }
                    return false;
                })
                user.save(function(err, usr){
                    res.send(usr);
                })
            }else{
                console.log("no one");
                res.send({})
            }
        });
    },

    findById: function(req, res){
        Users.findById(req.params.id).exec(function(err, professor){
            professor = professor || [];
            res.send(professor);
        });
    },

    update: function(req, res){
        if(req.body.password){
            var security = new Security(req.body.password);
            security.hash().then(function(){
                req.body.password = security.hash;
                Users.update({_id: req.params.id}, req.body, function(err, data){
                    var str = JSON.stringify({id: data._id, timestamp:moment().format('x')});
                    var token = jwt.sign(str, Config.secret, {});
                    res.send({token:token, email:req.body.email});
                });
            }, function(){
                delete req.body.password;
                Users.update({_id: req.auth_id}, req.body, function(err, data){
                    if(err){
                        res.send({error:err});
                    }else{
                        res.send(data);
                    }
                });
            });
        }else{
            Users.update({_id: req.auth_id}, req.body, function(err, data){
                if(err){
                    res.send({error:err});
                }else{
                    res.send(data);
                }
            });
        }
    },

    saveDedication: function(req, res){
        var key = Object.keys(req.files).pop();
        var file = req.files[key];
        var dir = '/var/www/client/app/assets/audio/' + req.auth_id + '/';
        var source = fs.createReadStream(file.path);
        var fname = file.name;
        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir);
        }
        var dest = fs.createWriteStream(dir + fname);

        source.pipe(dest);
        source.on('end', function() {
            fs.unlink(file.path, function(){
                res.send({});
            })
        })
    },

    saveRecordingInvite: function(req, res){
        var key = Object.keys(req.files).pop();
        var file = req.files[key];
        var dir = '/var/www/client/app/assets/audio/' + req.auth_id + '/';
        var source = fs.createReadStream(file.path);
        var fname = uuid.v1() + '.m4a';
        if (!fs.existsSync(dir)){
            console.log(dir);
            fs.mkdirSync(dir);
        }
        var dest = fs.createWriteStream(dir + fname);

        source.pipe(dest);
        source.on('end', function() {
            fs.unlink(file.path, function(){
                var k = key.replace('.m4a', '');
                Users.findOne({_id: req.auth_id}).populate('invitations_recieved').exec(function(err, rec){
                    var data;
                    var user;
                    rec.invitations_recieved.forEach(function(row){
                        if(row.uid == req.params.id){
                            user = row.user;
                            row.breaks = JSON.parse(req.headers["x-page-breaks"]);
                            row.file = req.auth_id + '/' + fname;
                            row.completed = true;
                            row.times_shared = 1;
                        }
                    })
                    Users.findOne({_id: user}).populate('invitations_sent').exec(function(err, sender){

                        sender.invitations_sent.forEach(function(row){
                            if(row.uid == req.params.id){
                                user = row.user;
                                row.file = req.auth_id + '/' + fname;
                                row.breaks = JSON.parse(req.headers["x-page-breaks"]);
                                row.completed = true;
                            }
                        })

                        rec.save(function(err, done){
                            sender.save(function(err, done){
                                res.send({});
                            });
                        });
                    });
                });
            });
        });
        source.on('error', function(err) {
            res.send({msg: 'error'});
        });
    },

    shareRecordingInvite: function(req, res){
        var story = req.params.id;
        var uid = req.body.uid;

        var send = function(sender, rec){
            if(!rec.stories_recieved){
                rec.stories_recieved = [];
            }

            if(!rec.stories_recieved){
                rec.stories_recieved = [];
            }

            if(!sender.stories_shared){
                sender.stories_shared = [];
            }

            if(!rec.circle){
                rec.circle = [];
            }

            if(!sender.circle){
                sender.circle = [];
            }

            var allowedToShare = true;
            var indx = 0;
            var i = -1;
            sender.invitations_recieved.forEach(function(r, idx){
                if(r.uid == uid){
                    i = idx;
                }
            });

            sender.stories_shared.forEach(function(r, idx){
                if(r.uid == uid){
                    indx++;
                }
            });

            indx++;

            if(indx > 2){
                allowedToShare = false;
            }

            sender.invitations_recieved[i].times_shared = indx;

            var file = sender.invitations_recieved[i].file;
            var story = sender.invitations_recieved[i].story;

            if(allowedToShare){

                if(rec.circle.indexOf(sender._id) == -1){
                    rec.circle.push(sender._id);
                }

                if(sender.circle.indexOf(rec._id) == -1){
                    sender.circle.push(rec._id);
                }

                var msg = "<p>You have been invited to view a story on Story Snap.</p>";
                if(req.body.message != ""){
                    msg = "You have a been invited to view a story on Story Snap by " + req.body.name;
                    msg += '<p style="background:#eeeeee; padding:5px;">' + decodeURIComponent(req.body.message) + "</p>";
                }
                msg += "<p>If you have any technical questions, please email: contact@storysnap.org</p>";

                var data = {
                    from: 'no-reply@storysnap.org',
                    to: req.body.email,
                    subject: 'A story has been shared with you on Story Snap',
                    html: msg
                };

               var s = {
                    story: story,
                    user: sender._id,
                    file: file,
                    breaks: JSON.parse('[' + req.body.breaks + ']'),
                    message: req.body.message || "",
                    fromName: req.body.name || "",
                    uid: uid
                };

                var s2 = {
                    story: story,
                    user: rec._id,
                    file: file,
                    breaks: JSON.parse('[' + req.body.breaks + ']'),
                    uid: uid
                };

                try{
                    if(rec.deviceTokens){
                        var tokens = rec.deviceTokens;
                        var note = new apn.Notification();
                        note.badge = 1;
                        note.sound = "ping.aiff";
                        note.alert = sender.first_name + ' ' + sender.last_name + " has shared a story with you.";
                        note.payload = {'messageFrom': 'StorySnap'};
                        apnConnection.pushNotification(note, tokens);
                    }
                }catch(e){
                    console.log(e);
                }

                rec.stories_recieved.push(s);
                sender.stories_shared.push(s2);
                rec.save(function(err, r){
                    sender.save(function(err2, r2){
                        console.log(r2);
                        Mailgun.messages().send(data, function (err, body) {
                            res.send({});
                        });
                    });
                });
            }else{
                sender.invitations_recieved[i].times_shared = 3;
                sender.save(function(err2, r2){
                    console.log('share limit');
                    res.send({msg:"You can only share a story 3 times."});
                });
            }
        }

        Users.findOne({email:req.body.email}, function(err, rec){
            Users.findOne({_id:req.auth_id}, function(err, sender){
                if(rec && sender){
                    send(sender, rec);
                }else{
                    console.log('creating new user');
                    var model = new Users();
                    var n = moment();
                    model.email = req.body.email;
                    model.first_name = "";
                    model.last_name = "";
                    model.active = false;
                    model.password = uuid.v1();
                    model.created = moment().format('x');
                    model.externalUser = true;
                    send(sender, model);
                }
            });
        });
    },

    setProfileImage: function(req, res){
        var file = req.files.file;
        var source = fs.createReadStream(file.path);
        var fname2 = file.name.toLowerCase().replace(/\s/g, '-').trim() + '-tmp.jpg';
        var fname = file.name.toLowerCase().replace(/\s/g, '-').trim() + '.jpg';
        //var dest1 = fs.createWriteStream('/var/www/client/app/assets/profiles/' + fname2);
        //var dest = fs.createWriteStream('/var/www/client/app/assets/profiles/' + fname);
        var dest = '/var/www/client/app/assets/profiles/' + fname;

        /*source.pipe(dest1);
        source.on('end', function() {
            fs.unlink(file.path, function(){*/
                gm(file.path).resize(1024, 1024).gravity('Center').write(dest, function(){
                   fs.unlink(file.path, function(){
                       console.log('here');
                        res.send(fname);
                    });
                });
        /*    })
        });
        source.on('error', function(err) {
            res.send({msg: 'error'});
        });*/
    },

    setProfileImagePublic: function(req, res){
        if(req.body.isGood = "true"){
            var file = req.files.file;
            var source = fs.createReadStream(file.path);
            var fname2 = file.name.toLowerCase().replace(/\s/g, '-').trim() + '-tmp.jpg';
            var fname = file.name.toLowerCase().replace(/\s/g, '-').trim() + '.jpg';
            //var dest1 = fs.createWriteStream('/var/www/client/app/assets/profiles/' + fname2);
            //var dest = fs.createWriteStream('/var/www/client/app/assets/profiles/' + fname);
            var dest = '/var/www/client/app/assets/profiles/' + fname;

            /*source.pipe(dest1);
            source.on('end', function() {
                fs.unlink(file.path, function(){*/
                    gm(file.path).resize(1024, 1024).gravity('Center').write(dest, function(){
                       fs.unlink(file.path, function(){
                           console.log('here');
                            res.send(fname);
                        });
                    });
            /*    })
            });
            source.on('error', function(err) {
                res.send({msg: 'error'});
            });*/
        }else{
            res.send({msg: 'error'});
        }
    },

    genCircles: function(req, res){
        Users.find({}, function(err, user){
            var u = Object.create(user);

            var next = function(){
                if(u.length > 0){
                    var us = u.pop();
                    if(!us.circle){
                        us.circle = [];
                    }

                    us.stories_recieved.forEach(function(s){
                         if(us.circle.indexOf(s.user) == -1){
                             us.circle.push(s.user);
                         }
                    });

                    us.invitations_recieved.forEach(function(s){
                         if(us.circle.indexOf(s.user) == -1){
                             us.circle.push(s.user);
                         }
                    });

                    us.stories_shared.forEach(function(s){
                         if(us.circle.indexOf(s.user) == -1){
                             us.circle.push(s.user);
                         }
                    });

                    us.invitations_sent.forEach(function(s){
                         if(us.circle.indexOf(s.user) == -1){
                             us.circle.push(s.user);
                         }
                    });

                    us.save(function(err, u2){
                        next();
                    })

                }else{
                    res.send({})
                }
            }

            next()
        });
    },

    inviteNewUser: function(req, res){
        var story = req.params.id;
        var send = function(sender, rec){
            var data = {
                from: 'no-reply@storysnap.org',
                to: req.body.email,
                subject: 'A story has been sent to you on Story Snap',
                html: "You have been invited to read a story with Story Snap."
            };

            if(!rec.invitations_recieved){
                rec.invitations_recieved = [];
            }

            if(!sender.invitations_sent){
                sender.invitations_sent = [];
            }

            if(!rec.circle){
                rec.circle = [];
            }

            if(!sender.circle){
                sender.circle = [];
            }

            if(rec.circle.indexOf(sender._id) == -1){
                rec.circle.push(sender._id);
            }

            if(sender.circle.indexOf(rec._id) == -1){
                sender.circle.push(rec._id);
            }

            var unique_id = req.body.uid;

            var inviteSent = {
                story: story,
                user: rec._id,
                completed: false,
                accepted: false,
                declined: false,
                uid: unique_id
            };

            var indx = 0;
            sender.invitations_sent.forEach(function(row, idx){
                if(row.uid == unique_id){
                    indx = idx;
                }
            })

            sender.invitations_sent[indx].declined = false;

            var inviteReceived = {
                story: story,
                user: sender._id,
                needToPurchase: sender.invitations_sent[indx].needToPurchase,
                message: req.body.message,
                completed: false,
                accepted: false,
                declined: false,
                uid: unique_id
            };

            if(rec.deviceTokens){
                var tokens = rec.deviceTokens
                var note = new apn.Notification();
                note.badge = 1;
                note.sound = "ping.aiff";
                note.alert = sender.first_name + ' ' + sender.last_name + " has invited you to read a story.";
                note.payload = {'messageFrom': 'StorySnap'};
                apnConnection.pushNotification(note, tokens);
            }

            rec.invitations_recieved.push(inviteReceived);
            rec.save(function(err, r){
                console.log(err);
                sender.save(function(err, r){
                    console.log(err);
                    Mailgun.messages().send(data, function (err, body) {
                        console.log(err);
                        res.send({});
                    });
                });
            });
        }

        Users.findOne({email:req.body.email}).populate('invitations_recieved').exec(function(err, rec){
            Users.findOne({_id:req.auth_id}).populate('invitations_sent').exec(function(err, sender){
                if(rec && sender){
                    send(sender, rec);
                }else{
                    console.log('creating new');
                    var model = new Users();
                    var n = moment();
                    model.email = req.body.email;
                    model.first_name = "";
                    model.last_name = "";
                    model.active = false;
                    model.password = uuid.v1();
                    model.created = moment().format('x');
                    model.externalUser = true;

                    send(sender, model);
                }
            });
        });
    },

    rating: function(req, res){
        Users.findOne({_id:req.auth_id}, function(err, user){
            Stories.findOne({_id:req.auth_id}, function(err, user){
                if(user){
                    res.send({confirmed: user.confirmed, pin: user.pin || "", _id: user._id});
                }else{
                    res.send({confirmed: -1, pin: "", _id: ""});
                }
            });
        });
    }
};
