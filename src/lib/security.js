var Bcrypt = require('bcryptjs');
var Promise = require('promise');
var Config = require('../config/config');
var jwt    = require('jsonwebtoken');

function Security(password){
	this.password = password;
	this.hash;
	this.SALT_WORK_FACTOR = 10;
}

Security.verify = function(token){
	return new Promise(function (resolve, reject){
        jwt.verify(token, Config.secret, function(err, decoded){
            if(err){
                reject();
            }
            resolve(decoded);
        });
	});
};

Security.prototype.salt = function(){
	var self = this;
	return new Promise(function (resolve, reject){
		Bcrypt.genSalt(self.SALT_WORK_FACTOR, function(err, salt) {
			console.log(salt);
			if(err){
				console.log('Salt: ' + err);
				reject(err);
			}
			self.salt = salt;
			resolve(salt);
		});
	});
};

Security.prototype.hash = function(){
	var self = this;
	return new Promise(function (resolve, reject){
		Bcrypt.hash(self.password, Config.salt, function(err, hash) {
			if(err){
				console.log('Hash: ' + err);
				reject(err);
			}
			self.hash = hash;
			resolve(hash);
		});
	});
};

Security.prototype.compare = function(pass){
	var self = this;
	return new Promise(function (resolve, reject){
		Bcrypt.compare(self.password, pass, function(err, isMatch) {
			if(err) {
				console.log('Match :' + err);
				reject(err);
			}
			resolve(isMatch);
		});
	});
};

module.exports = Security;
