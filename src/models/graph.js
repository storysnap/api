var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.model('Edge', {
    id: Schema.Types.ObjectId,
    label: String,
    source: {
        id: Schema.Types.ObjectId,
        label: String,
        weight: Number
    },
    target: {
        id: Schema.Types.ObjectId,
        label: String,
        weight: Number
    },
    data: {}
});

mongoose.model('Vertex', {
    id: Schema.Types.ObjectId,
    label: String,
    data: {}
});
