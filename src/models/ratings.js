var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: 'Users'},
    story: {type: Schema.Types.ObjectId, ref: 'Stories'},
    rating: {type: Number, default: 1},
    date: {type: Date, default: Date.now},
    type: { type: String, index: true }
});

mongoose.model('Ratings', schema);
