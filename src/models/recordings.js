var moment = require('moment');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    story: {type: Schema.Types.ObjectId, ref: 'Stories'},
    user: {type: Schema.Types.ObjectId, ref: 'Users'},
    recorded_date: {type: Number, default: moment().format('x')},
    timestamp: Number,
    shares: {type: Number, default: 3}
});

mongoose.model('Recordings', schema);
