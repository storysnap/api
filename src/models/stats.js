var mongoose = require('mongoose');
var Schema = mongoose.Schema;


//Types

var types = {
    "Account Creation": 0,
    "Story Favorited": 1,
    "Story Purchase": 2,
    "Story Record Launch": 3,
    "Completion of Record": 4,
    "Shared Story": 5,
    "Invitation of Non-User to install": 6,
    "Invitation of a non-user to read story": 7,
    "Reminder of Invitation": 8,
    "Withdrawl of Invitation": 9,
    "Stories received for review": 10,
    "Stories reviewed and accepted": 11,
    "Inviation received": 12,
    "Reminder received": 13,
    "Inviation accepted": 14,
    "Inviation declined": 15,
    "Story Playback": 16,
    "Export audio to MP3": 17,
    "Session Start": 18,
    "Session End": 19,
    "Details Viewed": 20,
    "Tips turned off": 21,
    "Tips turned on": 22
}
/*var schema = new Schema({
    action: {type:Number, default: 0, index: true},
    fromUser: {type: Schema.Types.ObjectId, ref: 'Users'},
    toUser: {type: Schema.Types.ObjectId, ref: 'Users'},
    story: {type: Schema.Types.ObjectId, ref: 'Stories'},
    date: {type: Date, default: Date.now, index: true},
    queueId: mongoose.Schema.Types.ObjectId,
    prePaid: Boolean,
    uid: String
});*/

var schema = new Schema({
    type: String,
    total: Number,
    count: Number,
    activity: [
        {
            action: {type:Number, default: 0, index: true},
            fromUser: {type: Schema.Types.ObjectId, ref: 'Users'},
            toUser: {type: Schema.Types.ObjectId, ref: 'Users'},
            story: {type: Schema.Types.ObjectId, ref: 'Stories'},
            date: {type: Date, default: Date.now, index: true},
            queueId: mongoose.Schema.Types.ObjectId,
            prePaid: Boolean,
            uid: String
        }
    ]
});

mongoose.model('Stats', schema);


/*var Stats = mongoose.model('UserStats', {
    total_users: Number,
    total_accounts_activated: Number,
    total_users_purchased: Number,
    total_users_no_action: Number,
    total_users_recorded: Number,
    total_users_shared: Number,
    total_users_sent_invite: Number,
    total_users_full_profile: Number,
    date: String,
    sort: Number
});

mongoose.model('Stats', Stats);*/
