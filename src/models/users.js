var mongoose = require('mongoose');
var Schema = mongoose.Schema;


//Types

//0: Stories to record
//1: Invitations received
//2: Invitations sent
//3: Recordings by others
//4: Recordings by me

var schema = new Schema({
    first_name:  String,
    last_name: String,
    email: {type: String, required: true, unique: true},
    title: String,
    date: {type: Date, default: Date.now},
    lastActive: {type: Date, default: Date.now},
    dateActived: {type: Date, default: Date.now},
    password: {type: String, select: false},
    pin: {type:String, default: "0000", select: false},
    recoverToken:String,
    publisher: String,
    active: {type: Boolean, default: true},
    confirmed: {type: Boolean, default: false},
    deviceTokens: [String],
    country: String,
    languages: [String],
    birth_year: String,
    favorite_books: String,
    favorite_books_to_read: String,
    profileImage: {type: String, default:"default.png"},
    externalUser: {type: Boolean, default: false},
    numberOfSessions: {type: Number, default: 0},
    totalTime: {type: Number, default: 0},

    favorites: [{type: Schema.Types.ObjectId, ref: 'Stories'}],

    queue: [
        {
            story: {type: Schema.Types.ObjectId, ref: 'Stories'},
            breaks: [Number],
            file: {type: String, default: ""},
            times_shared: {type: Number, default: 0},
            shared_with: [{
                user: {type: Schema.Types.ObjectId, ref: 'Users'},
                date: {type: Date, default: Date.now}
            }],
            purchase_date: {type: Date, default: Date.now},
            date: {type: Date, default: Date.now},
            type: {type: Number, default: 0},
            type_state: {type: Number, default: 0},
            completed_date: Date,
            user: {type: Schema.Types.ObjectId, ref: 'Users'},
            readerName: {type: String, default: ""},
            message: {type: String, default: ""},
            uid: {type: String, default: ""},
            dedication: {type: String, default: ""},
            fromName: {type: String, default: ""},
            accepted: {type: Boolean, default: false},
            completed: {type: Boolean, default: false},
            declined: {type: Boolean, default: false},
            withdrawn: {type: Boolean, default: false},
            needToPurchase: {type: Boolean, default: false},
            viewed: {type: Boolean, default: false},
            archived: {type: Boolean, default: false},
            times_recorded: {type: Number, default: 0},
            times_playback: {type: Number, default: 0},
        }
    ],

    ratings: [
        {
            story: {type: Schema.Types.ObjectId, ref: 'Stories'},
            date: {type: Date, default: Date.now},
            rating: Number
        }
    ],

    receipts: [{
        story: {type: Schema.Types.ObjectId, ref: 'Stories'},
        date: {type: Date, default: Date.now}
    }],

    circle: [{type: Schema.Types.ObjectId, ref: 'Users'}],

    activity: [{
        action: String,
        story: {type: Schema.Types.ObjectId, ref: 'Stories'},
        date: {type: Date, default: Date.now}
    }],


    previews: [{type: Schema.Types.ObjectId, ref: 'Stories'}],
    takes: [{story_id: {type: Schema.Types.ObjectId, ref: 'Stories'}, page: Number, take: Number, timestamp: {type: Date, default: Date.now}}],
    search_log: [String]

});

mongoose.model('Users', schema);
