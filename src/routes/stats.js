var Router = require('blklab-server').Router;
var Controller = require('../controllers/stats');

Router.get('/stats', Controller.totals);

Router.get('/stats/users', Controller.users);

Router.get('/stats/stories', Controller.stories);

Router.get('/stats/filter/:filter', Controller.filterBy);
