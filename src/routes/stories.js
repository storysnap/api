var Router = require('blklab-server').Router;
var Controller = require('../controllers/stories');

Router.get('/stories', Controller.find, true);

Router.get('/stories/list', Controller.findProducts, true);

Router.get('/stories/thumbnails', Controller.findThumbnails, true);

Router.get('/stories/discover', Controller.discover, true);

Router.get('/stories/all', Controller.findAll);

Router.get('/stories/filtered-by/:filter', Controller.findFiltered, true);

Router.get('/stories/:id', Controller.findById);

Router.post('/stories', Controller.create);

Router.put('/stories/:id', Controller.update);

Router.post('/stories/create/featured/:id', Controller.createFeatured);

Router.post('/stories/create/thumbnails/:id', Controller.createThumb);

Router.post('/stories/optimize/:id', Controller.optimizeStory);

Router.post('/stories/process-assets', Controller.process);

Router.post('/stories/process-assets/:id', Controller.process);
