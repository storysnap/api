var Router = require('blklab-server').Router;
var Controller = require('../controllers/users');

//Manipulate Data

Router.post('/users/purchase/:id', Controller.purchase, true);

Router.post('/users/favorite/:id', Controller.favorite, true);

Router.del('/users/favorite/:id', Controller.removeFavorite, true);

Router.post('/users/share/:id', Controller.shareRecording, true);

Router.post('/users/share/invite/:id', Controller.shareRecordingInvite, true);

Router.post('/users/share/accept/:id', Controller.acceptShare, true);

Router.post('/users/invite/revoke', Controller.revokeInvite, true);

Router.post('/users/invite/remind', Controller.remindInvite, true);

Router.post('/users/invite/accept', Controller.acceptInvite, true);

Router.post('/users/invite/decline', Controller.declineInvite, true);

Router.post('/users/pin', Controller.sendPin, true);

Router.post('/users/launched-story/:id', Controller.launchedStory, true);

Router.post('/users/preview-story/:id', Controller.previewStory, true);

Router.post('/users/launched-app', Controller.launchedApp, true);

Router.post('/users/closed-app', Controller.closedApp, true);

//Get Data

Router.get('/users/library', Controller.library, true);

Router.get('/users/circle', Controller.circle, true);

Router.get('/users/purchases', Controller.purchases, true);

Router.get('/users/purchases/:id', Controller.purchasesById, true);

Router.get('/users/search', Controller.search, true);






Router.get('/users/kids', Controller.kids, true);

Router.get('/users/profile', Controller.profile, true);

Router.get('/users/recordings', Controller.recordings, true);

Router.get('/users/invitations', Controller.invitations, true);

Router.get('/users/shared', Controller.shared, true);

Router.get('/users/auth/verified', Controller.isVerified, true);

Router.post('/users/auth/verify/:id', Controller.verify);

Router.put('/users/auth/device', Controller.setDevice, true);

Router.put('/users', Controller.update, true);

Router.put('/users/profile-image', Controller.setProfileImage, true);

Router.put('/users/public/profile-image', Controller.setProfileImagePublic);

Router.put('/users/archive/story/:id', Controller.archive, true);

Router.get('/users/auth/logout', Controller.logout);

Router.get('/users/gen/circles', Controller.genCircles);

Router.post('/users/auth/login', Controller.login);

Router.post('/users/auth/forgot', Controller.sendRecover);

Router.post('/users/auth/reset/:token', Controller.reset);

Router.post('/users', Controller.create);

Router.post('/users/dedication', Controller.saveDedication, true);

Router.post('/users/recordings/:id', Controller.saveRecording, true);

Router.post('/users/recordings/invite/:id', Controller.saveRecordingInvite, true);

Router.post('/users/invite/:id', Controller.inviteUser, true);

Router.post('/users/invite/resend/:id', Controller.inviteDifferentUser, true);

Router.post('/users/invite/new/:id', Controller.inviteNewUser, true);

Router.post('/users/rating/:id', Controller.rating, true);
